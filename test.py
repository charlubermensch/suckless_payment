"""Run unittests."""

import unittest

from io import StringIO

from unittest.mock import patch

from os import remove
from os.path import isfile

from src.lib import (
    crash,
    get_lock_path,
    get_locked_addresses,
    get_address_parameter
)

from src.run_del import write_addresses, unlock_address

from src.new import lock_address, is_address_locked


class BaseTest(unittest.TestCase):
    def test_crash(self):
        with patch("sys.stderr", new=StringIO()) as fake_out:
            try:
                crash("test")
            except SystemExit as e:
                error = e
            self.assertEqual(fake_out.getvalue(), "error: test\n")
            self.assertIsInstance(error, SystemExit)
            self.assertEqual(error.code, 1)

    def test_address_paramater_fail(self):
        with patch("sys.stderr", new=StringIO()) as fake_out:
            try:
                get_address_parameter(["sp.py", "com"])
            except SystemExit as e:
                error = e
            self.assertEqual(fake_out.getvalue(), "error: no input address\n")
            self.assertIsInstance(error, SystemExit)

    def test_address_paramater_successs(self):
        address = get_address_parameter(["sp.py", "com", "Bf5EFoXdsYmVs"])
        self.assertEqual(address, "Bf5EFoXdsYmVs")


LOCK_PATH = get_lock_path()


class LockedTest(unittest.TestCase):
    """
        Tests that mess around with `~/.local/share/sp_locked`.

        Do not use if the file is used.
    """
    def test_empty(self):
        if isfile(LOCK_PATH):
            remove(LOCK_PATH)
        addresses = get_locked_addresses()
        self.assertEqual(addresses, [])

    def test_one(self):
        with open(LOCK_PATH, "wb") as file:
            file.write(b"address\n")
        addresses = get_locked_addresses()
        self.assertEqual(addresses, ["address"])

    def test_two(self):
        with open(LOCK_PATH, "wb") as file:
            file.write(b"address\nother\n")
        addresses = get_locked_addresses()
        self.assertEqual(addresses, ["address", "other"])

    def test_write_addresses_none(self):
        write_addresses([])

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"")
        self.assertEqual(adds, [])
        self.assertEqual(len(adds), 0)

    def test_write_addresses_one(self):
        write_addresses(["address"])

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"address\n")
        self.assertEqual(adds, ["address"])
        self.assertEqual(len(adds), 1)

    def test_write_addresses_two(self):
        write_addresses(["one", "two"])

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"one\ntwo\n")
        self.assertEqual(adds, ["one", "two"])
        self.assertEqual(len(adds), 2)

    def test_write_unlock(self):
        write_addresses(["address"])

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"address\n")
        self.assertEqual(adds, ["address"])
        self.assertEqual(len(adds), 1)

        unlock_address("address")

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"")
        self.assertEqual(adds, [])
        self.assertEqual(len(adds), 0)

    def test_lock_unlock(self):
        # clean up
        write_addresses([])

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"")
        self.assertEqual(adds, [])
        self.assertEqual(len(adds), 0)

        # lock
        lock_address("address")

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"address\n")
        self.assertEqual(adds, ["address"])
        self.assertEqual(len(adds), 1)

        # unlock
        unlock_address("address")

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"")
        self.assertEqual(adds, [])
        self.assertEqual(len(adds), 0)

    def test_write_is_locked(self):
        # write address
        write_addresses(["address"])

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"address\n")
        self.assertEqual(adds, ["address"])
        self.assertEqual(len(adds), 1)

        # verify is locked
        is_locked = is_address_locked("address")
        self.assertEqual(is_locked, True)

    def test_write_is_not_locked(self):
        # write address
        write_addresses([])

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"")
        self.assertEqual(adds, [])
        self.assertEqual(len(adds), 0)

        # verify is locked
        is_locked = is_address_locked("address")
        self.assertEqual(is_locked, False)

    def test_lock_unlock_is_locked(self):
        # clean up
        write_addresses([])

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"")
        self.assertEqual(adds, [])
        self.assertEqual(len(adds), 0)

        # lock
        lock_address("address")

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"address\n")
        self.assertEqual(adds, ["address"])
        self.assertEqual(len(adds), 1)

        is_locked = is_address_locked("address")

        self.assertEqual(is_locked, True)

        # unlock
        unlock_address("address")

        adds = get_locked_addresses()

        is_file = isfile(get_lock_path())
        self.assertEqual(is_file, True)

        with open(get_lock_path(), "rb") as file:
            b_file = file.read()

        self.assertEqual(b_file, b"")
        self.assertEqual(adds, [])
        self.assertEqual(len(adds), 0)

        is_locked = is_address_locked("address")

        self.assertEqual(is_locked, False)


if __name__ == "__main__":
    unittest.main(exit=True, verbosity=3)
