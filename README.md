# ⚠️ This software has many things you must be warned about:
1. According to the country you're in, e.g. 🇷🇺 Russia, it may be **illegal** to use it
2. Unlike [Shadowchat](https://github.com/LukeSmithxyz/shadowchat), this software requires to use a **local blockchain**, which takes **days** to download and hundreds of gigabits
3. This is still a draft, use it only you will be able to change things up once a serious, i.e. non-Python, version comes out

<div align="center">

<img src="https://gitlab.com/uploads/-/system/project/avatar/33680024/opti_gimp_sp.png" width="200">

# SucklessPayment (sp)

[![License: GPL v2](https://img.shields.io/badge/License-GPL_v2-blue.svg)](https://tldrlegal.com/license/gnu-general-public-license-v2)

A payment processor of Monero aiming [suckless](https://suckless.org)ness.


[Getting started](#getting-started) •
[Usage](#usage) •
[Integration](#integration) •
[Further](#further)


</div>


<img src="https://charlubermensch.com/images/record.svg"></img>


# Preparation and usage

### Getting started

Download the testnet blockchain.

```
monerod --testnet
```

*N.B: you should keep it running in background*

Create a wallet on this blockchain:

```
monero-wallet-cli --testnet --generate-new-wallet testwallet
```

Launch a walelt

```
monero-wallet-rpc --testnet --wallet-file testwallet --password "" --rpc-bind-port 28088 --disable-rpc-login
```

*N.B: you should also keep it running in background in order that the code accesses it*

You may want to keep your `~` clean. In this case, run the second command in somewhere else because the commands generates `monero-wallet-cli.log`, `monero-wallet-rpc.log`, `testwallet`, `testwallet.address.txt` and `testwallet.keys` in the working directory.

One may use `.local/cache/sp` as an "offset directory" or a `.cache` added to the `.gitignore` in the code directory. This may be standardize in the future.

It's the moment where the repo should be copied:

```
git clone https://gitlab.com/charlubermensch/suckless_payment.git
cd suckless_payment
```

Then run

```
virtualenv .venv
source .venv/bin/activate
```

Install the dependencies

```
pip install monero
```

Installing the module is better explained and more in depth in the [API documentation](https://monero-python.readthedocs.io/en/latest/quickstart.html).

### Preparation every time

Run in a terminal

```
monerod --testnet
```

*N.B: it will download the block it hasn't downloaded yet for the blockchain*

And in another

```
monero-wallet-rpc --testnet --wallet-file testwallet --password "" --rpc-bind-port 28088 --disable-rpc-login
```

### Usage

The software supports only three commands:

```
python sp.py new
```

Write to standart output a new address empty address.

```
python sp.py get [--unlocked] <address>
```

Write to standart output the balance of the address.

```
python sp.py del <address>
```

This command send all the money of the sub-address to the main address. (priority of the transaction is in `Further knowledge`)

# Documentation

In addition to this README, there is a man page

```
groff -Tascii -man sp.man | less
```

This command has to be run when being in repo directory

```
git clone https://gitlab.com/charlubermensch/suckless_payment.git
cd suckless_payment
```

# Tests

Run the tests with

```
python test.py
```

# Integration

In order to be used with other tools, e.g. [TelegramPayment](https://gitlab.com/charlubermensch/telegrampayment), the software has to be compiled and put into `$PATH`.

## Compilation

Open the virtual environment

```
virtualenv .venv
source .venv/bin/activate
```

Install `pyinstaller`

```
pip install pyinstaller
```

Compile in one file

```
pyinstaller --onefile sp.py
```

## Moving the file

For testing, one may put it in `~/.local/bin`

```
cp dist/sp ~/.local/bin/
```

But as you may think, for serious use in server, it should be better to put it in `/usr/bin/`

```
cp dist/sp /usr/bin/
```

One may clean one's directory afterwards

```
rm -r dist build dist.spec
```

# Further

<details>
<summary>Features</summary>

## Required

- [x] new - only to get already existenct empty sub-address
- [x] get - get a subaddress balance
- [x] del - free sub-address

## Questionable featurse

- [x] new - create a new subaddresses no empty sub-address found
- [x] get - ```--unlocked``` option to see unlocked balance
- [ ] cashback - gives back if too many money has been sent
- [ ] del - ```--ignore-balance``` and ```--ignore-file``` to specify if should crash when the balance isn't good or the address isn't in ```~/.local/share/sp_locked```

</details>

<details>
<summary>Roadmap</summary>

- [x] `add` and `del` commands
- [x] Add `get` command
- [x] Integrate with TelegramPayment
- [x] Add man page
- [x] Add test-driven-development
- [x] Clean up README
- [ ] Clarify fee, multithreading and the code itself
- [ ] Add (bloat) features
- [ ] Integrate with a django website
- [ ] Maybe add more for support (Matrix channel, ...)

</details>

<details>
<summary>Contributing</summary>

Run `pylama` in order to check for a good code quality. <br>
Avoid adding features. <br>
Avoid a coding style which isn't the one of the author. (no OO, no f-string, avoid editing the `pylama.ini`'s `ignore`)

The author has a webpage about his [coding style](https://charlubermensch.com/articles/programming/).

Also unittests are to be added.

The project has been writen in Python in accordance to the skills of the author and in order to make a draft fast. However, there will be a transition to other languages, i.e. Go, C(++), Shell Script, Forth, ...

</details>

<details>
<summary>Further knowledge</summary>

## Fee/Priority

One can edit the priority when the money is transfered from a sub-address. Higher is higher in speed but in price also. As of now, editing it requires to edit the code itself. In future, this may be eased with a global variable or even putted to a `config.py`.

## Number of sub-addresses (=parralel payments)

At the beginning, sub-addresses were "hand-made" by the user. (see old `README.md`) But since the sub-addresses diseappeared by themselves, the author sadly had to add a feature: it's made automatically when running `python sp.py new`.

As of now, in order to change this one has to edit `src/new.py` and change the global variable at the top `MAX_SUBADDRESSES`. This may be moved to `config.py` but the more suckless way would be to find a way that the software hasn't to manage that.

## Having multiple sp call at once

The code itself shouldn't have much issue with that. However, the software depends on a socket and a file (`~/.cache/sp_locked`). One can't do much with the socket except managing better its errors. For the file however, one may use `fuser` to verify that the file isn't used and thereby avoid any conflict, making that `~/.cache/sp_locked` is edited by the programs one by one. Contrarly to what I believed files can be not only read by multiple programs but **also being writen**.

## Preventive error

There may be in the future preventive errors in case the sub-address is empty and isn't locked, .i.e hasn't been used.

</details>

## Authors and acknowledgment
The only author is as of now [Charl'Übermensch](https://charlubermensch.com).

## License
This code is licensed under the GNU General Public License version 2 or later. (SPDX: GPL-2.0-or-later)

Reuse of the code is possible under GPLv3 (or later) but the author is willing to use the GPLv2 for the software itself.

This is free software: you are fre eto change and redistribute it. There is NO WARRANTY, to the extent permitted by law.

The module which the software is based upon has for him a [BSD 3-Clause License](https://monero-python.readthedocs.io/en/latest/license.html).

## Project status
The software isn't ready yet for "production" but is almost.

It's the main priority of the author as of now.
