.TH SP 7 "15 February 2022"
.SH NAME
sp \- Commands with processing Monero payments as purpose
.SH SYNOPSIS
.SY "python sp.py"
.OP new/get/del
.OP address
.YS
.SH COMMANDS
.TP
.B new
.br
Write to standart output an unlocked empty sub-address and locks it.
.RS
.P
When no empty sub-addresses is found, make a new sub-address unless maximum subaddresses number is reached.
.P
This number can be edited in \fIsrc/new.py\fP by changing the global variable \fIMAX_SUBADDRESSES\fP.
.P
Locking the address means to put it in \fI~/.local/share/sp_locked\fP.
.RE
.TP
.B get
.OP --unlocked
.I <address>
.br
Write to standart output the balance of sub-address <address>.
.RS
.PP
.OP --unlocked
specifies that the the balance has to be the unlocked(=confirmed) one.
.PP
Before the parameter was ahead of the address, but now it has to be at the end.
.RE
.TP
.B del
.I <address>
.br
Send all the money of the sub-address to the main address and unlocks it.
.RS
.P
Unlocking the address does the reverse of \fBnew\fP, i\.e\. removing it from \fI~/.local/share/sp_locked\fP.
.P
Whether the software should crash when the balance is empty or the address isn't in \fI~/.local/share/sp_locked\fP is subject to discussion.
.P
Sending the money is a transaction, the fee, i\.e\. speed, of it is subject to discussion also.
.RE
.SH PREREQUISITE
.B FIRST TIME
.RS
.PP
Download the testnet blockchain.
.RS
.PP
monerod --testnet
.RE
.PP
Create a wallet on this blockchain:
.RS
.PP
monero-wallet-cli --testnet --generate-new-wallet testwallet
.RE
.PP
Launch a wallet
.RS
monero-wallet-cli --testnet --generate-new-wallet testwallet
.RE
.PP
Launch a virtual environment
.RS
.PP
virtualenv .venv
.br
source .venv/bin/activate
.RE
.PP
Install the dependencies
.RS
.PP
pip install monero
.RE
.RE
.B EVERY TIME

.RS

.PP
Run in a terminal that will stay in background
.RS
.PP
monerod --testnet
.RE

.PP
And in another
.RS
.PP
monero-wallet-rpc --testnet --wallet-file testwallet --password "" --rpc-bind-port 28088 --disable-rpc-login
.RE

.RE

.SH EXAMPLES
.TP
python sp.py new
.RS
.PP
Bf5EFoXdsYmVspGgj8N6fmi4T1khXxYXngA4sNVRJMkFeWbDfLHtED8KoLmnB9UHHoG2XBCDvPhQ4aeScMms2EdS7qYJmag
.RE
.TP
python sp.py get Bf5EFoXdsYmVspGgj8N6fmi4T1khXxYXngA4sNVRJMkFeWbDfLHtED8KoLmnB9UHHoG2XBCDvPhQ4aeScMms2EdS7qYJmag
.RS
.PP
0.0
.RE
.TP
python sp.py del Bf5EFoXdsYmVspGgj8N6fmi4T1khXxYXngA4sNVRJMkFeWbDfLHtED8KoLmnB9UHHoG2XBCDvPhQ4aeScMms2EdS7qYJmag
.RS
.PP
empty address
.RE
.RE
.SH "REPORTING BUGS"
GitLab issues: <https://gitlab.com/charlubermensch/suckless_payment/-/issues>
.SH BUGS
ValueError usually comes from a non-number entry where there should be one. Double check the file you edited. You may also convert your float into integer.
.SH "SEE ALSO"
python(1)
.SH AUTHOR
Written by "Charl'Ubermensch" (pseudonyme).
.LP
.UR https://gitlab.com/charlubermensch
Remote Git repositories
.UE
.LP
.MT inlife.developper@tutanota.com
CharlUbermensch
.ME
.br
.MT contact@charlubermensch.com
CharlUbermensch
.ME
.SH COPYRIGHT
License GPLv2: GNU General Public License version 2.
<https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html>
.PP
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
