"""Run sys.argv[1] command."""

import sys

from src.lib import crash

from src.new import new
from src.run_del import run_del
from src.get import get


def run():
    """Run sys.argv[1] command."""
    if len(sys.argv) == 1:
        print("error: no input command", file=sys.stderr)
        sys.exit(1)

    if sys.argv[1] == "new":
        new()
        return

    if sys.argv[1] == "del":
        run_del()
        return

    if sys.argv[1] == "get":
        get()
        return

    crash("%s is not a known command" % sys.argv[1])


if __name__ == "__main__":
    run()
