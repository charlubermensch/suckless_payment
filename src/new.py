"""`sp new` command."""

import sys

from monero.wallet import Wallet

from monero.backends.jsonrpc import JSONRPCWallet

from src.lib import get_lock_path, get_locked_addresses, crash

MAX_SUBADDRESSES = 3


def lock_address(address: str):
    """Write address to locked addres file."""
    b_address = bytes(address + "\n", encoding="utf-8")
    with open(get_lock_path(), "ab") as file:
        file.write(b_address)


def is_address_locked(address: str) -> bool:
    """Check if an address is locked."""
    return address in get_locked_addresses()


def get_empty_address(wallet: Wallet) -> str:
    """
        Return an unlocked empty address.

        Return empty string if not found.
    """
    for acc in wallet.accounts[1:]:
        balance_decimal = acc.balance()
        if not balance_decimal.is_zero():
            continue

        address = str(acc.address())
        if is_address_locked(address):
            continue

        return address
    return ""


def use_address(address: str):
    """Show, lock address and exit 0."""
    print(address)
    lock_address(address)
    sys.exit(0)


def check_empty_address(wallet: Wallet):
    """Check for an empty subaddress."""
    address = get_empty_address(wallet)

    if address:
        use_address(address)


def make_new_address(wallet: Wallet):
    """Make a new sub-account if it won't exceed `MAX_ADDRESSES`."""
    if len(wallet.accounts) >= MAX_SUBADDRESSES:
        return

    account = wallet.new_account()
    address = str(account.address())

    use_address(address)


def new():
    """`sp new` command."""
    wallet = Wallet(JSONRPCWallet(port=28088))

    check_empty_address(wallet)

    make_new_address(wallet)

    crash("no empty address has been found nor can be made")
