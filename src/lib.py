"""A common library between commands."""

import sys

from os.path import isfile

from os import environ

from monero.wallet import Wallet

from monero.account import Account


def crash(error_msg: str):
    """Print at `sys.stderr` error: `error_msg` and exit 1."""
    print("error: %s" % error_msg, file=sys.stderr)
    sys.exit(1)


def get_account_from_address(wallet: Wallet, address: str) -> Account:  # pylint: disable=R1710
    """Return sub-account of `wallet` with as main address `address`."""
    for acc in wallet.accounts:
        if str(acc.address()) == address:
            return acc

    crash("\"%s\" address not found" % address)


def get_lock_path() -> str:
    """Return ~/.local/share/sp_locked path."""
    return "%s/.local/share/sp_locked" % environ["HOME"]


def get_locked_addresses() -> list[str]:
    """Return a list of all the locked addresses."""
    if not isfile(get_lock_path()):
        return []

    with open(get_lock_path(), "rb") as file:
        b_addresses = file.read()
    s_addresses = str(b_addresses, encoding="utf-8")
    pre = [a.strip() for a in s_addresses.split("\n")]
    return [a for a in pre if a]


def get_address_parameter(argv: list) -> str:
    """Return the address given in the `sys.argv` or crash if not found."""
    if len(argv) == 2:
        crash("no input address")

    return argv[-1]
