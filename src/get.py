"""Show sub-address balance."""

import sys

from monero.wallet import Wallet

from monero.backends.jsonrpc import JSONRPCWallet

from src.lib import get_account_from_address, get_address_parameter


def get():
    """Show sub-address balance."""
    address = get_address_parameter(sys.argv)

    wallet = Wallet(JSONRPCWallet(port=28088))

    account = get_account_from_address(wallet, address)

    print(float(account.balance("--unlocked" in sys.argv[2:])))
