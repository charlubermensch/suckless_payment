"""Make an address newly available by transfering its balance and unlocking it."""

import sys

from os.path import isfile

from monero.wallet import Wallet

from monero.backends.jsonrpc import JSONRPCWallet

from src.lib import (
    get_account_from_address,
    get_lock_path,
    get_locked_addresses,
    get_address_parameter
)


def write_addresses(addresses: list[str]):
    """Write `addresses` in `~/.local/share/sp_locked`."""
    buffer = ""
    for address in addresses:
        buffer += address + "\n"
    b_buffer = bytes(buffer, encoding="utf-8")
    with open(get_lock_path(), "wb") as file:
        file.write(b_buffer)


def unlock_address(address: str):
    """Unlock `address`."""
    if not isfile(get_lock_path()):
        return

    addresses = get_locked_addresses()

    if address in addresses:
        addresses.remove(address)

    write_addresses(addresses)


def run_del():
    """
        Make an address newly available by transfering its balance and unlocking it.

        The name of the function is `run_del()` instead of `del()` since
        `del` is a reserved Python keyword.
    """
    address = get_address_parameter(sys.argv)

    wallet = Wallet(JSONRPCWallet(port=28088))

    sub_account = get_account_from_address(wallet, address)

    if not sub_account.balance().is_zero():
        sub_account.sweep_all(wallet.address())
    else:
        print("empty address")

    unlock_address(address)
